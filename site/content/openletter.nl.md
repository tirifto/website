---
title: Open Brief
type: page
layout: subpage
sigtable: true
---

Publiek gefinancierde software moet [Vrije en Open Bron Software][fs] zijn.
Hoewel hier een overvloed van goede redenen voor is, weten vele politici nog
niet hoe belangrijk software is.

{{< fsdefinition type="box">}}

U kunt hiermee helpen!  Onderteken de open brief zodat onze boodschap nog luider
overkomt, zoals 15 mensen en 21 organisaties reeds gedaan hebben.  Wij zullen de
brief en handtekeningen overhandigen aan uw vertegenwoordigers om ervoor te
zorgen dat zij begrijpen: Publiek Geld?  Publieke Code!

> ## Publiek Geld?  Publieke Code!
>
> Digitale diensten die gebruikt en aangeboden worden door onze openbare
> besturen zijn dé cruciale infrastructuur van 21e-eeuwse democratische landen.
> Om betrouwbare systemen te vestigen, moeten overheidsinstanties volledig
> beheer hebben over de software en computersystemen in het hart van onze
> digitale infrastructuur.  Echter is dat huidig meestal niet het geval vanwege
> beperkende softwarelicenties die:
>
> * Het delen en uitwisselen van publiek gefinancierde code verbieden.  Dit
>   maakt samenwerking tussen openbare besturen onmogelijk en verhindert
>   doorontwikkeling.
>
> * Monopolies ondersteunen door concurrentie te belemmeren.  Veel besturen
>   worden hierdoor afhankelijk van een handjevol bedrijven.
>
> * Een bedreiging vormen voor de veiligheid van onze digitale infrastructuur
>   door toegang tot de broncode te verbieden.  Dit maakt het moeilijk of zelfs
>   onmogelijk om achterdeurtjes en veiligheidsgaten te dichten.
>
> We hebben software nodig die het delen van goede ideeën en oplossingen
> bevordert.  Op deze manier kunnen we ICT-diensten verbeteren voor mensen in
> heel Europa.  We hebben software nodig die garant staat voor vrijheid van
> keuze, toegang en concurrentie.  We hebben software nodig die openbare
> besturen helpt om volledig beheer over hun cruciale digitale infrastructuur te
> herwinnen, zodat ze onafhankelijk kunnen worden en blijven van een handjevol
> bedrijven.  Daarom doen wij een oproep aan onze vertegenwoordigers om Vrije en
> Open Bron Software te ondersteunen in openbare besturen.  Omdat:
>
> * Vrije en Open Bron Software is een modern, openbaar goed dat iedereen in
>   staat stelt om de applicaties waar we dagelijks gebruik van maken in
>   vrijheid te gebruiken, te bestuderen, te delen en te verbeteren.
>
> * Vrije en Open Bron Softwarelicenties waarborgen tegen afhankelijkheid van
>   diensten van specifieke bedrijven die beperkende licenties gebruiken om
>   concurrentie te verhinderen.
>
> * Vrije en Open Bron Software garandeert dat de broncode toegankelijk is,
>   zodat achterdeurtjes en veiligheidsgaten gedicht kunnen worden zonder
>   afhankelijk te zijn van een enkele dienstverlener.
>
> Overheidsinstanties worden gefinancierd door belastingen. Zij moeten ervoor
> zorgen dat het geld zo efficiënt mogelijk besteed wordt.  Als het publiek geld
> is, dan zou het ook publieke code moeten zijn!
>
> Daarom roepen wij, de ondergetekenden, onze vertegenwoordigers op:
>
> “Creëer wetgeving die vereist dat publiek gefinancierde software ontwikkeld
> voor de publieke sector openbaar beschikbaar gesteld moet worden onder een
> Vrije en Open Bron Softwarelicentie.”

<!--- De onderstaande mensen hebben de brief al ondertekend en hebben toegestemd
om hun handtekening openbaar te maken.  Zult u de volgende zijn? --->

[fs]: https://fsfe.org/freesoftware/basics/summary.nl.html "Vrije Software geeft iedereen het recht om software te gebruiken, te begrijpen, aan te passen en te delen. Deze rechten helpen het ondersteunen van andere fundamentele vrijheden zoals de vrijheid van meningsuiting, pers en privacy."
